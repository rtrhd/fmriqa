Moving things around to conform better with a normal python package.

- fmriqa package
  - fmriqa python routines
  - fbirnqa qt application - need to reconsider names ...

How do we do the resource business. For the packaged version we need a way to automatically compile the resources and add them to
the installed package directory. We can import them from a relative path. The entry for the scipt can be via the usal stub mechanism.

For the 'standalone' script we still need to explicitly merge the resources in the python script file as before.

For testing we can assume the package based layout, but we still need to make sure the resources can be imported without an error
before the package is actually installed. Alternatively we could make the system 'safe' from failing to import the resources - the
tests aren't going to exercise the gui much anyway and we could have fallback buttons.

Want

 - way to specify the resource compilation step in setup.py
 - Makefile to create resources in package directory or don't raise on failure to import.
 - Makefile still to create standalone script with merged resources
 - see if there is acleaner way to merge the resources

Maybe we could have the `Makefile` run `setup.py build` and find a way of making that do the resource compilation.
Then we could pick the files out of the build directory and do the merge.

Will also still need to

 - find a way of having test data at accessible location (too big to put in repo really)
 - document dependencies
 - sort out way of putting patches to bxh code in repo (or copy bxh code an put in github repo if licence allows)
 - update installation of web app and see if there's a cleaner way to do this (with flash!)

With Anaconda we have an upcoming problem with the move to [Qt5](http://doc.qt.io/qt-5/qt5-intro.html).

Do we add some conditional imports for [PyQt4](http://pyqt.sourceforge.net/Docs/PyQt4/) vs
[PyQt5](http://pyqt.sourceforge.net/Docs/PyQt5/)
(as well as possibly [PySide](https://wiki.qt.io/PySide)) or do we use an abstraction layer
like [qtpy](https://github.com/spyder-ide/qtpy)?

The main difference in the imports is that most of what was in QtGui is now in QtWidgets.
We would also need to run the Qt5 resource builder.

Looks like it's rather difficult to modify setuptools to run a build step.
We can either do something like:

```
import setuptools.command.build_py
import distutils.cmd

class CompileResources(distutils.cmd.Command):
    description = 'build qt resources from rc file'

    def initialize_options(self):
        self.resourcefiles = glob('*.rcs') # ouch what's our pwd ? we want this relative to package directory

    def finalize_options(self):
        pass

    def run(self):
        """Run command."""
        command_base = ['pyrcc4']
        command_base += ['-py3'] if PY3 else ['-py2']
        for f in self.resourcefile:
            command = command_base + ['-o', f.replace('.rcs', '.py'), f]
            self.announce('Running command: %s' % str(command), level=distutils.log.INFO)
            subprocess.check_call(command)
 
class BuildWithResources(setuptools.command.build_py.build_py):
    def run(self):
        self.run_command('build_res')
        setuptools.command.build_py.build_py.run(self)


setuptools.setup(
    cmdclass={
        'compile_res': CompileResources,
        'build_py': BuildWithResources,
    },
    # Usual setup() args.
    # ...
)
```

If we put imagedir under package and rc file in package then we could run rule to compile any resources to python.

We'd need to decide how we name resource files. Usually it could be related to module name.
Or we could compile all resources in a given directory?
How do we make sure the compiled resources end up with the modules in the build area?

The easiest thing would be to compile the resources and version them (ugh), perhaps with a make target
for rebuilding them.

Maybe we'll try that for now.

we'll call the module fbirnqa.py and the script fbirnqa.
the resources will be fbirnqa_resources.py
import will have to be by relative import
standalone istallation will be by merge as now but no compilation step
move the resources under a subdir and add a compile Makefile
top lavel makefile will copy into package directory
This should be enough. We'll want a main() entry point for the command

Another problem is that we'll need different versions of the compiled resources depending on python23 x qt4/qt5/pyside!
That's a total of 6 we'll need to maintain and choose which one at install time (ugh).

How does setuptools know which to install? We need some python code to execute to determine which toolkit is available in the
build environment.

The possibilities are:
```
pyrcc4 -py2/-py3 -o out.py in.qrc
pyrcc5 -o out.py in.qrc
pyside-rcc -py2/-py3 -o out.py in.qrc
```

Looks as if `pyrcc5` is python 3 style only, whereas `pyrcc4` and `pyside-rcc` support both.

It looks as if the only difference in the python 3 style is the use of the 'b' prefix on the strings.
Also, `pyrcc4` and  `pyside-rcc` differ in that the latter puts the strings on one line.
Other than that they look the same so as python 2.6/2.7 supports the 'b' we are OK with just the python 3 version.

Suggest, in Makefile, we use `pyrcc4` or `pyrcc5` and ignore `pyside`. Then there's no need for setup.py to know
anything about the resource version. Actually we'll wrap this up in a script `pyrcc-generic` which will run whatever is
available an always produce python3 compatible code - note that we haven't really validated the `pyside` branch as we don't
have a test environment.

Issues with testing - cannot import fmriqa.fbirnqatool if resources not build. Would like to make this optional
for testing but currently we replace only the single import line. If we had a try block we would need to remove
the whole thing. Look at merge script. Would need fancier sed magic compatible with osx as well - leave for now.

OK -Done

Still TODO:
 - set up git-lfs in bitbucket for test data files
 - sort out installation of anaconda versions without installing the gui script (ideally lose module entirely)
 - bring fmriqa symbols up to top level so clients do not have to change import line
 - fix issues of how to get right version of dcmfetch; delete dependency for anaconda python
 - clean up notebooks and make sure they still run
 - check web stuff still works
 - add more unit tests
 - add reference results for regression tests
 - add markdown manual with references to original papers and details of phantom (embedded pics?)
 - installation instructions including bxh stuff + test install on macos x
 - check new qt5 web widgets on macos X - DONE
 - cut a release candidate
 - merge with master
 - open repository

September 2016

 - now use qtpy for qt library independence

single version string - problematical for the case of standalone installation.
we need to replace the `from . version import __version__` with the literal text
from version.py.
so we need a patch_version script. OK DONE
Outstanding issues:
 - afni version coming out wrong - FIXED (sort of)
 - want a --version command line flag
 
Problem in analysis script for Philips scans (fwhm_smoothness_xyz_preprocessed->phantom_mask_3d->phantom_mask_3d->threshold_otsu->histogram->np.bincount). Image is uint64 but wants to convert to int64.
Need to find out why bincount can't cope with uint64 and force to int64 somewhere in fmriqa.  

