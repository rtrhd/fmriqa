# -*- coding: utf-8 -*-
#!/usr/bin/python

import sys
import numpy as np
from PyQt4 import QtGui, QtCore

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import fmriqa




class FilePicker(QtGui.QWidget):
    """
    An example file picker
    """

    def __init__(self, parent=None):
        super(FilePicker, self).__init__(parent)
        self.dname= None
        
        # Set the window dimensions
        #self.resize(300,75)
        
        # vertical layout for widgets
        self.vbox = QtGui.QVBoxLayout()
        self.setLayout(self.vbox)

        # Create a label which displays the path to our chosen file
        self.lbl = QtGui.QLabel('No directory selected')
        self.vbox.addWidget(self.lbl)

        # Create a push button labelled 'choose' and add it to our layout
        btn = QtGui.QPushButton('Choose directory', self)
        self.vbox.addWidget(btn)
        
        # Connect the clicked signal to the get_fname handler
        self.connect(btn, QtCore.SIGNAL('clicked()'), self.get_dname)
        
        # want to signal the main window here that we have a dataset to analyse
        #

    def get_dname(self):
        """
        Handler called when 'choose directory' is clicked
        """
        dname = QtGui.QFileDialog.getExistingDirectory(self, 'Select directory')
        if dname and dname != self.dname:
            self.lbl.setText(dname)
            self.emit(QtCore.SIGNAL('seriesdirchanged'), dname)
            self.dname = dname



class FMRIResultsPanel(QtGui.QFrame):
    def __init__(self, parent=None):
        super(FMRIResultsPanel, self).__init__(parent)
        # A grid with labels and fixed text fields


class FMRIControlPanel(QtGui.QFrame):
    def __init__(self, parent=None):
        super(FMRIControlPanel, self).__init__(parent)
        
        self.directory_choice = FilePicker(self)
        self.directory_choice_lbl = QtGui.QLabel('Run Analysis')

        self.drop_frames = QtGui.QWidget()        
        self.start_button = QtGui.QPushButton('Run')
        # A collection of buttons etc with actions
        
        grid = QtGui.QGridLayout()
        grid.addWidget(self.directory_choice, 0, 0)
        self.connect(self.directory_choice, QtCore.SIGNAL('seriesdirchanged'),
                     self.seriesdirchanged)
        grid.addWidget(self.start_button, 1, 1)
        
        #grid.addWidget(self.directory_choice_lbl, 0, 0)

                
        """
        j = 0
        pos = [(0, 0), (0, 1), (0, 2), (0, 3),
                (1, 0), (1, 1), (1, 2), (1, 3),
                (2, 0), (2, 1), (2, 2), (2, 3),
                (3, 0), (3, 1), (3, 2), (3, 3 ),
                (4, 0), (4, 1), (4, 2), (4, 3)]


        for i in names:
            button = QtGui.QPushButton(i)
            if j == 2:
                grid.addWidget(QtGui.QLabel(''), 0, 2)
            else:
                grid.addWidget(button, pos[j][0], pos[j][1])
            j = j + 1
        """

        self.setLayout(grid)
    
    def seriesdirchanged(self, dname):
        self.emit(QtCore.SIGNAL('seriesdirchanged'), dname)

        
class FMRIPlottingTabs(QtGui.QTabWidget):
    def __init__(self, parent=None):
        super(FMRIPlottingTabs, self).__init__(parent)
        # Tabs will be added dynamically when analysis is done 
        self.tabs = [(FMRIPlottingWidget(self), "Plots")]
        
        tab_indices = [self.addTab(widget, name)
            for (widget, name) in self.tabs]
        

class FMRIPlottingWidget(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100, fig=None):
        if fig is None:
            self.fig = Figure(figsize=(width, height), dpi=dpi)
            FigureCanvas.__init__(self, self.fig)
        else:
            self.fig = fig

        self.axes = self.fig.add_subplot(111)

        # We want the axes cleared every time plot() is called
        self.axes.hold(False)
            
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)
     
        self.compute_initial_figure()
        
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)

        FigureCanvas.updateGeometry(self)


    def compute_figure(self):
        self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')


   
class MeanSignalImagePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None

        # Mean Signal Image                    
        self.axes.imshow(fmriqa.signal_image(time_series))
        self.axes.axis('off')
        self.axes.title('Signal Image')
        self.axes.colorbar()


class TNFIImagePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None

        # Fluctuation Noise Image                    
        self.axes.imshow(fmriqa.temporalnoise_fluct_image(time_series))
        self.axes.axis('off')
        self.axes.title('Temporal Fluctuation Image')
        self.axes.colorbar()
        

class SFNRImagePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None

        # Signal to Fluctuation Noise Ratio Image
        sfnri = fmriqa.sfnr_image(time_series)
        sfnri[sfnri>500] = 500                    
        self.axes.imshow(sfnri)
        self.axes.axis('off')
        self.axes.title('SFNR Image')
        self.axes.colorbar()

class SSNIImagePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None

        # Static Spatial Noise Image                  
        self.axes.imshow(fmriqa.static_spatial_noise_image(time_series))
        self.axes.axis('off')
        self.axes.title('Static Spatial Noise Image')
        self.axes.colorbar()


class TimeCoursePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None
        
        time_course = fmriqa.roi_means_time_course(time_series, roisize=15)
        nt = len(time_course)                       
        (a, b, c) = np.polyfit(xrange(nt), time_course, deg=2)
        trend = np.polyval((a, b, c), xrange(nt))       
        self.axes.plot(np.arange(nt), time_course, 'r+-', label='observed')
        self.axes.plot(np.arange(nt), trend, 'b-', label='fit')           

        sd, fluct, drift_raw, drift_fit = fmriqa.fluctuation_and_drift(
                                          time_series, roisize=15)                                          
        summary_vals = (fluct, drift_raw, drift_fit)
        self.axes.title('[%%fluct, drift, driftfit] = [%.2f %.2f %.2f]' % summary_vals)            
        self.axes.xlabel('Frame Number')
        self.axes.ylabel('Raw Signal (ROI)')
        self.axes.legend() 

class DetrendedTimeCoursePlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None
        
        time_course = fmriqa.roi_means_time_course(time_series, roisize=15)
        detr_time_course = fmriqa.detrended_roi_time_course(
                                 time_series, roisize=15)
        nt = len(detr_time_course)                     
        (a, b, c) = np.polyfit(xrange(nt), time_course, deg=2)
        trend = np.polyval((a, b, c), xrange(nt)) 
        self.axes.plot(np.arange(nt), 100*detr_time_course/trend, '+-')
        self.axes.title('Detrended Time Course')
        self.axes.xlabel('Frame Number')
        self.axes.ylabel('Percentage Deviation')  
 

class MagnitudeSpectrumPlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series, delta_t):
        self.time_series = time_series
        self.delta_t = delta_t
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self,time_series=None, delta_t=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series
        if delta_t is None:
            delta_t = self.delta_t
        else:
            self.delta_t = delta_t
        assert time_series is not None
        assert delta_t is not None


        spectrum = fmriqa.magnitude_spectrum(time_series, roisize=15)
        nf = len(spectrum)
        self.axes.plot(np.linspace(0, (nf-1)/delta_t/nf/2, num=nf), spectrum, '+-')

        summary_vals = (fmriqa.signal_summary(self.time_series, roisize=15),
                        fmriqa.snr_summary(self.time_series, roisize=15),
                        fmriqa.sfnr_summary(self.time_series, roisize=15))
                  
        self.axes.title('[Mean, SNR, SFNR] = [%.1f %.1f %.1f]' % summary_vals)
        self.axes.xlabel('Frequency (Hz)')
        self.axes.ylabel('Magnitude Spectrum (mean scaled)')


class PowerSpectrumPlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series, delta_t):
        self.time_series = time_series
        self.delta_t = delta_t
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None, delta_t=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series
        if delta_t is None:
            delta_t = self.delta_t
        else:
            self.delta_t = delta_t
        assert time_series is not None
        assert delta_t is not None
        
        time_course = fmriqa.roi_means_time_course(time_series,
                                                roisize=15)            
        self.axes.psd(time_course/np.mean(time_course), len(time_course),
                      1/self.delta_t, detrend=fmriqa.detrend_quadratic)           
        self.axes.plot()
        self.axes.title('Power Spectrum')
            

class WeisskoffPlot(FMRIPlottingWidget):
    def __init__(self, fig, time_series):
        self.time_series = time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, time_series=None):
        if time_series is None:
            time_series = self.time_series
        else:
            self.time_series = time_series

        assert time_series is not None
        
        # Weisskoff Plot
        roc, covs = fmriqa.weisskoff(time_series, max_roisize=15)

        self.axes.loglog(range(1, len(covs)+1),
                         100*covs[0] / np.arange(1, len(covs)+1),
                         'r-', label = 'calc.')
        self.axes.loglog(range(1, len(covs)+1), 100*covs, 'b+-', label='meas.')
        self.axes.title('Weisskoff Plot roc = %.2f pixels' % roc)
        self.axes.xlabel('ROI Width')
        self.axes.ylabel('100*COV')
        self.axes.legend()



class PositionDriftPlot(FMRIPlottingWidget):
    def __init__(self, fig, vol_time_series):
        self.vol_time_series = vol_time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, vol_time_series=None):
        if vol_time_series is None:
            vol_time_series = self.vol_time_series
        else:
            self.vol_time_series = vol_time_series

        assert vol_time_series is not None
            
        # Image Position Drift
        cofg = fmriqa.centre_of_mass(vol_time_series)
        self.axes.subplot(4, 4, 14)           
        self.axes.plot(range(1, len(cofg)+1),
                       [x-cofg[0][0] for (x,y,z) in cofg], '+-', label='X')
        self.axes.plot(range(1, len(cofg)+1),
                       [y-cofg[0][1] for (x,y,z) in cofg], '+-', label='Y')
        self.axes.plot(range(1, len(cofg)+1),
                       [z-cofg[0][2] for (x,y,z) in cofg], '+-', label='Z')
        self.axes.title('Relative Position')
        self.axes.xlabel('Frame No') 
        self.axes.ylabel('X, Y, Z Position (Pixels)')
        self.axes.legend()



class GhostinessPlot(FMRIPlottingWidget):
    def __init__(self, fig, vol_time_series):
        self.vol_time_series = vol_time_series
        FMRIPlottingWidget.__init__(self, fig)

    def compute_figure(self, vol_time_series=None):
        if vol_time_series is None:
            vol_time_series = self.vol_time_series
        else:
            self.vol_time_series = vol_time_series

        assert vol_time_series is not None
        
        # Ghostiness statistics
        pmeans, gmeans, bright_gmeans, snrs = fmriqa.ghostiness_trends(vol_time_series)          
        self.axes.plot(range(1, len(gmeans)+1), 100*gmeans/pmeans, '+-', label='Ghosts')
        self.axes.plot(range(1, len(bright_gmeans)+1), 100*bright_gmeans/pmeans, '+-', label='Bright Ghosts')
        self.axes.title('Ghostiness')
        self.axes.xlabel('Frame No') 
        self.axes.ylabel('Relative Intensity (%)')
        self.axes.legend()
            
            
      
class FMRICentralWidget(QtGui.QFrame):
    def __init__(self, parent=None):
        super(FMRICentralWidget, self).__init__(parent)
        # add control Panel, and Plotting tab
        self.control_panel = FMRIControlPanel(self)
        self.results_panel = FMRIResultsPanel(self)
        self.plotting_tabs = FMRIPlottingTabs(self)
        
        vbox = QtGui.QVBoxLayout()
        for w in [ self.control_panel, self.results_panel ]:
            vbox.addWidget(w)
            #vbox.setAlignment(w, QtCore.AlignHCenter)
            
        hbox = QtGui.QHBoxLayout()
        hbox.addLayout(vbox)
        hbox.addWidget(self.plotting_tabs)
        self.setLayout(hbox)



class FMRIMainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(FMRIMainWindow, self).__init__(parent)

        self.setWindowTitle('Bristol fMRI QA Analysis')

        self._create_menu()
        self._create_status_bar()

        self.dirname = None
        central_widget = FMRICentralWidget(self)
        self.setCentralWidget(central_widget)

        self.connect(self.centralWidget().control_panel,
                     QtCore.SIGNAL('seriesdirchanged'),
                     self.seriesdirchanged)

        self.connect(self.centralWidget().control_panel.run_button,
                     QtCore.SIGNAL('clicked()'), self.run_clicked)


    def run_clicked(self):
        if self.dname is not None:
            self.vol_time_series, delta_t = fmriqa.read_mosaic_time_series(self.dname, '*.dcm')
            
            # Select Slice and time points to use in analysis
            # fBIRM seems to choose 24 (base 0) as the middle slice
            # and drop the 1st 2 time pts if no.of time pts even or 3 if odd
            nt, nz, ny, nx = self.vol_time_series.shape
            tstart = 2 + nt%2
            zmiddle = nz//2
            self.slice_time_series = self.vol_time_series[tstart:,zmiddle,:,:]
            
            tabbed_widget.addTab(,'')

            ## want to loop over all the analysis
            
        else:
            QtGui.QMessageBox()
       
    def seriesdirchanged(self, dname):
        self.dname = dname
        print "new dir is", dname

    
    def on_about(self):
        msg = """ fMRIQA using PyQt4 and matplotlib:
        
         * Use the matplotlib navigation bar
         * Add values to the text box and press Enter (or click "Draw")
         * Show or hide the grid
         * Drag the slider to modify the width of the bars
         * Save the plot to a file using the File menu
         * Click on a bar to receive an informative message
        """
        QtGui.QMessageBox.about(self, "About fMRIQA", msg.strip())
        

    def _create_status_bar(self):
        self.status_text = QtGui.QLabel("This is a demo")
        self.statusBar().addWidget(self.status_text, 1)

        
    def _create_menu(self):
        # File Menu
        self.file_menu = self.menuBar().addMenu("&File")

        load_data_series_action = self._create_action("&Load data series",
            shortcut="Ctrl+L", slot=self.load_data_series, tip="Load fMRI data")
            
        save_results_action = self._create_action("&Save results",
            shortcut="Ctrl+S", slot=self.save_results, tip="Save analysis results")

        quit_action = self._create_action("&Quit", slot=self.close, 
            shortcut="Ctrl+Q", tip="Close the application")
        
        self._add_actions(self.file_menu,
                [load_data_series_action, save_results_action, None, quit_action])

        # About Menu
        self.help_menu = self.menuBar().addMenu("&Help")
        about_action = self._create_action("&About", 
            shortcut='F1', slot=self.on_about, tip='About the fMRIQA')
        
        self._add_actions(self.help_menu, [about_action])


    def _add_actions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def _create_action(self, text, slot=None, shortcut=None, 
                       icon=None, tip=None, checkable=False, 
                       signal="triggered()"):
            
        action = QtGui.QAction(text, self)
        if icon is not None:
            # TODO: how do we attache icon resources?
            action.setIcon(QtGui.QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, QtCore.SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action

    def load_data_series():
        pass
        
    def save_results():
        pass

"""        
class Example(QtGui.QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        
    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Icon')
        self.setWindowIcon(QtGui.QIcon('web.png'))        
        self.show()
"""

def main():
    app = QtGui.QApplication(sys.argv)
    # seems this dummy assignment is needed
    mw = FMRIMainWindow()
    mw.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    

