#!/usr/bin/env python

"""
Python CGI script to generate google chart based report on fMRI QA results. Should be placed in
in an executable script area of the web server on canopus. Requires the google DataTable python library
which is available from "http://code.google.com/p/google-visualization-python/" and is installed via setup.py.
Access to postgres is which the standard DBAPI module for postgres 'pyscopg2'.
Remember to grant select access on the results table to 'apache'
"""

from __future__ import division, print_function, absolute_import

# Ridiculously, we need this to get the default locale for dates
import locale
locale.setlocale(locale.LC_TIME, '')

import datetime as dt
import gviz_api
import psycopg2 as pg
import cgi

# cgi debugging traceback
import cgitb
cgitb.enable()

# These are the defaults for CRIC Bristol - change as required
DBNAME = 'fMRIQAResults'
DBHOST = 'canopus'
SCANNER = 'cric45064'


def fetch_as_json(start_date, scanner):
    '''Fetch historical frmi QA data starting at a given date.
       Returns a json structure suitable for a motionchart.
    '''
    # By convention the DB table name will be the scanner id preceded by 'qa'
    tablename = 'qa' + scanner

    # The fields that are tracked
    FIELDS = [
        "percentFluc", "drift",     "driftfit",
        "mean",        "SNR",       "SFNR",
        "rdc",         "std",
        "minFWHMX",    "meanFWHMX", "maxFWHMX",
        "minFWHMY",    "meanFWHMY", "maxFWHMY",
        "minFWHMZ",    "meanFWHMZ", "maxFWHMZ",
        "dispCMassX",  "driftCMassX",
        "dispCMassY",  "driftCMassY",
        "dispCMassZ",  "driftCMassZ",
        "meanGhost",   "meanBrightGhost"
    ]

    LABELS = [
        "Fluctuation (%)", "Drift (%)",     "Drift (fitted) (%)",
        "Mean Signal",     "Static SNR","SFNR",
        "Radius of Decorrelation (pixels)",         "Standard Deviation",
        "Minimum X FWHM (pixels)",    "Mean X FWHM (pixels)", "Maximum X FWHM (pixels)",
        "Minimum Y FWHM (pixels)",    "Mean Y FWHM (pixels)", "Maximum Y FWHM (pixels)",
        "Minimum Z FWHM (pixels)",    "Mean Z FWHM (pixels)", "Maximum Z FWHM (pixels)",
        "X Displacement (mm)",  "X Drift (mm)",
        "Y Displacement (mm)",  "Y Drift (mm)",
        "Z Displacement (mm)",  "Z Drift (mm)",
        "Ghosts (%)",   "Bright Ghosts(%)"
    ]

    connection = pg.connect(database=DBNAME, host=DBHOST)
    cursor = connection.cursor()

    fields_str = ', '.join(FIELDS)
    query = 'SELECT scandate, %s FROM ' % fields_str + tablename + ' WHERE scandate >= %s' + ' ORDER BY scandate'

    cursor.execute(query, [start_date])
    results = list(cursor.fetchall())

    cursor.close()
    connection.close()
    description = [
        ('scanner', 'string', 'Scanner'), ('scandate', 'date', 'Scan Date')
    ] + [
        (field, 'number', label) for (field, label) in zip(FIELDS, LABELS)
    ]
    data_table = gviz_api.DataTable(description)
    data_table.LoadData([['CRICBristol'] + list(result) for result in results])

    # (ID [,data_type [,label [,custom_properties]]]
    json = data_table.ToJSon(columns_order=['scanner', 'scandate'] + FIELDS, order_by='scandate')
    return results[0][0], results[-1][0], json


# A template for the html page to be served
page_template = """
<html>
    <head>
    <title>Historical fMRI Stability Analysis Results</title>
        <script src="http://www.google.com/jsapi"></script>
        <script>
            google.load('visualization', '1', {packages: ['motionchart', 'annotatedtimeline']});

            function drawMotionChart() {
                var mcdata = new google.visualization.DataTable(%(json)s, 0.5);

                var motionchart = new google.visualization.MotionChart(
                  document.getElementById('motion-chart'));
                var options = {};
                options['state']  = '%(initialstate)s';
                options['width']  = 900;
                options['height'] = 600;
                motionchart.draw(mcdata, options);
            }
            function setScanner() {
                var scanners = document.getElementById("scanners");
                var scanner = scanners.options[scanners.selectedIndex].text;

            }

            function drawVisualizations() {
                drawMotionChart()
            }
            google.setOnLoadCallback(drawVisualizations);
        </script>
    </head>
    <body style="font-family: Arial;border: 0 none;">
        <h2>fMRI Stability QA - Historical Results from %(first)s to %(last)s for %(scanner)s</h2>
        <h3>Motion Chart View</h3>
        <p>The display below is a <em>motion chart</em>. It is set up initially to plot the percentage fluctuation
           against the date of the scan. The line is coloured according to the percentage drift.</p>
        <p>Other parameters can be plotted instead by selecting from the menus at the left and right of the
           the chart.</p>
        <p>Alternative visualizations are also possible by selecting a different chart type from the tabs at the top right of the plot.</p>
        <p>Note that it currently requires your browser to have a flash plugin installed.</p>
        <div id="motion-chart" style="width: 1200px; height: 600px;"></div>
    </body>
</html>
"""

initialstate = r'{"xZoomedDataMax":1352678400000,"colorOption":"3","showTrails":false,"yZoomedDataMax":0.1,"iconKeySettings":[],"yZoomedIn":false,"xAxisOption":"_TIME","xLambda":1,"iconType":"LINE","orderedByX":false,"time":"2012-11-12","playDuration":15000,"yZoomedDataMin":0,"xZoomedIn":false,"sizeOption":"_UNISIZE","yLambda":1,"uniColorForNonSelected":false,"xZoomedDataMin":1347235200000,"duration":{"timeUnit":"D","multiplier":1},"yAxisOption":"2","nonSelectedAlpha":0.4,"orderedByY":false,"dimensions":{"iconDimensions":["dim0"]}}'


def page(start_date, scanner):
    first, last, json = fetch_as_json(start_date, scanner)
    print(type(first))
    print(type(scanner))
    return page_template % {
        'json': json,
        'initialstate': initialstate,
        'first': first.strftime('%x'),
        'last': last.strftime('%x'),
        'scanner': scanner
    }


if __name__ == '__main__':
    form = cgi.FieldStorage()
    scanner = form.getvalue('scanner') if 'scanner' in form else SCANNER

    start_date = dt.datetime.strptime('2012-01-01', '%Y-%m-%d')
    print('Content-Type: text/html')
    print()
    print(page(start_date, scanner))
