# Basic Web Reports on QA History #
An executable python CGI script to generate google chart based report on fMRI QA results.
It should be placed in an executable script area of the web server.
it also requires the google DataTable python library
which is available from [here](http://code.google.com/p/google-visualization-python/) and is installed via  a distutils setup.py.
Access to postgres requires the standard DBAPI module for postgres `pyscopg2`.
Remember to grant select access on the results table to the user `apache` or equivalent.
There is also an example html web page which links it for two named MRI scaneers.
