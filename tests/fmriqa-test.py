from __future__ import division, print_function
import sys
import numpy as np
from hashlib import sha1

try:
    from pydicom import dcmread
except ImportError:
    from dicom import read_file as dcmread

from nose.tools import assert_equals, assert_almost_equals, assert_raises, assert_greater
from numpy.testing import assert_allclose

sys.path.insert(0, '..')
import fmriqa


mosaic_test_file        = 'testdata/siemens/dicom/006D5BAD.dcm'
mosaic_test_series      = 'testdata/siemens/dicom'

hybrid_test_file        = 'testdata/siemensxa/dicom/00300.dcm'
hybrid_test_series      = 'testdata/siemensxa/dicom'

multiframe_test_file    = 'testdata/philips/dicom/MR098EEEC6.dcm'
multiframe_test_series  = 'testdata/philips/dicom'

singleframe_test_file   = 'testdata/ge/dicom/MR23AD61F1.dcm'
singleframe_test_series = 'testdata/ge/dicom'


def test_number_of_frames():
    assert_equals(fmriqa.number_of_frames(dcmread(singleframe_test_file)), 1)
    assert_equals(fmriqa.number_of_frames(dcmread(mosaic_test_file)), 1)
    assert_equals(fmriqa.number_of_frames(dcmread(multiframe_test_file)), 16200)
    assert_equals(fmriqa.number_of_frames(dcmread(hybrid_test_file)), 31)


def test_is_multiframe():
    assert not fmriqa.is_multiframe(dcmread(mosaic_test_file))
    assert     fmriqa.is_multiframe(dcmread(multiframe_test_file))
    assert not fmriqa.is_multiframe(dcmread(singleframe_test_file))
    assert     fmriqa.is_multiframe(dcmread(hybrid_test_file))


def test_multiframe_ndims():
    assert_equals(fmriqa.multiframe_ndims(dcmread(multiframe_test_file)), 3)
    assert_equals(fmriqa.multiframe_ndims(dcmread(hybrid_test_file)), 3)


def test_multiframe_shape():
    assert_equals(fmriqa.multiframe_shape(dcmread(multiframe_test_file)), (1, 27, 600))
    assert_equals(fmriqa.multiframe_shape(dcmread(hybrid_test_file)), (1, 31, 1))


def test_is_enhancedmr():
    assert not fmriqa.is_enhancedmr(dcmread(singleframe_test_file))
    assert not fmriqa.is_enhancedmr(dcmread(mosaic_test_file))
    assert     fmriqa.is_enhancedmr(dcmread(multiframe_test_file))
    assert     fmriqa.is_enhancedmr(dcmread(hybrid_test_file))


def test_manufacturer():
    assert_equals(fmriqa.manufacturer(dcmread(singleframe_test_file)), 'GE')
    assert_equals(fmriqa.manufacturer(dcmread(mosaic_test_file)), 'Siemens')
    assert_equals(fmriqa.manufacturer(dcmread(multiframe_test_file)), 'Philips')
    assert_equals(fmriqa.manufacturer(dcmread(hybrid_test_file)), 'Siemens')


def test_is_mosaic():
    assert     fmriqa.is_mosaic(dcmread(mosaic_test_file))
    assert not fmriqa.is_mosaic(dcmread(multiframe_test_file))
    assert not fmriqa.is_mosaic(dcmread(singleframe_test_file))
    assert not fmriqa.is_mosaic(dcmread(hybrid_test_file))


def test_acquisition_time_seconds():
    assert_equals(fmriqa.acquisition_time_seconds(dcmread(singleframe_test_file)), 150558.0)
    assert_equals(fmriqa.acquisition_time_seconds(dcmread(mosaic_test_file)), 191123.0925)
    assert_equals(fmriqa.acquisition_time_seconds(dcmread(multiframe_test_file)), 417459833.26)
    assert_equals(fmriqa.acquisition_time_seconds(dcmread(hybrid_test_file)), 600785611.3175)


def test_meshgrid_3d():
    X, Y, Z = fmriqa.meshgrid_3d([0, 1, 2, 3], [0, 1, 2], [0, 1])
    assert X.shape == Y.shape == Z.shape == (2, 3, 4)
    assert np.array_equal(X, np.array(
            [[[0, 1, 2, 3],
              [0, 1, 2, 3],
              [0, 1, 2, 3]],
             [[0, 1, 2, 3],
              [0, 1, 2, 3],
              [0, 1, 2, 3]]]))

    assert np.array_equal(Y, np.array(
            [[[0, 0, 0, 0],
              [1, 1, 1, 1],
              [2, 2, 2, 2]],
             [[0, 0, 0, 0],
              [1, 1, 1, 1],
              [2, 2, 2, 2]]]))

    assert np.array_equal(Z, np.array(
            [[[0, 0, 0, 0],
              [0, 0, 0, 0],
              [0, 0, 0, 0]],
             [[1, 1, 1, 1],
              [1, 1, 1, 1],
              [1, 1, 1, 1]]]))


def test_stack_from_mosaic():
    dcmobj = dcmread(mosaic_test_file)
    _NumberOfImagesInMosaic = 0x0019,0x100a
    nimages = int(dcmobj[_NumberOfImagesInMosaic].value)
    stack = fmriqa.stack_from_mosaic(dcmobj, nimages)
    # Right shape
    assert_equals(stack.shape, (27, 64, 64))
    # No empty slices
    assert all(np.sum(np.abs(stack), axis=(1, 2)) > 0)

def test_quadratic_trend():
    x = np.linspace(-10, 10, 100)
    y = 3.0 * x**2 + 4.0 * x + 2.0 + np.random.normal(loc=0.0, scale=0.1, size=len(x))
    y_orig = y.copy()

    yy = fmriqa.quadratic_trend(y)
    assert np.abs(np.mean(yy - y)) < 0.01
    assert 0.05 < np.std(yy - y) < 0.2
    assert np.allclose(y, y_orig)


def test_detrend_quadratic0():
    x = np.linspace(-10, 10, 100)
    y = -5.0 * x**2 + 3.0 * x + 2.0 + np.random.normal(loc=0.0, scale=0.1, size=len(x))
    y_orig = y.copy()

    yy = fmriqa.detrend_quadratic(y)
    assert np.abs(np.mean(yy)) < 0.01
    assert 0.05 < np.std(yy) < 0.2
    assert np.allclose(y, y_orig)


def test_detrend_quadratic1():
    def _detrend_quadratic_simple(y):
        x = np.arange(len(y))
        return y - np.polyval(np.polyfit(x, y, deg=2), x)

    x = np.linspace(-10, 10, 100)
    y = -5.0 * x**2 + 3.0 * x + 2.0 + np.random.normal(loc=0.0, scale=0.1, size=len(x))
    y_orig = y.copy()
    yyref = _detrend_quadratic_simple(y)
    yy = fmriqa.detrend_quadratic(y)
    assert yy.shape == yyref.shape
    assert yy.dtype == yyref.dtype
    assert np.allclose(yy, yyref)
    assert np.allclose(y, y_orig)


def test_detrend_quadratic2():
    def _detrend_quadratic_simple(y):
        x = np.arange(len(y))
        return y - np.polyval(np.polyfit(x, y, deg=2), x)

    x = np.linspace(-10, 10, 100)

    y = np.array([
        ((-5.0 + 0.1 * k) * x**2 + 3.0 * x + 2.0)
        for k in range(5)
    ]).T
    y += np.random.normal(loc=0.0, scale=0.1, size=y.shape)
    y_orig = y.copy()

    yyref = np.zeros_like(y)
    for i in range(5):
        yyref[:, i] = _detrend_quadratic_simple(y[:, i])

    yy = fmriqa.detrend_quadratic(y)
    assert yy.shape == yyref.shape
    assert yy.dtype == yyref.dtype
    assert np.allclose(yy, yyref)
    assert np.allclose(y, y_orig)


def test_detrend_quadratic3():
    def _detrend_quadratic_simple(y):
        x = np.arange(len(y))
        return y - np.polyval(np.polyfit(x, y, deg=2), x)

    x = np.linspace(-10, 10, 100)
    y = np.array([
        [((-5.0 + 0.1 * k) * x**2 + (3.0 + 0.1 * j) * x + 2.0) for k in range(5)]
        for j in range(5)
    ]).T

    y += np.random.normal(loc=0.0, scale=0.1, size=y.shape)
    y_orig = y.copy()

    yyref = np.zeros_like(y)
    for i in range(5):
        for j in range(5):
            yyref[:, i, j] = _detrend_quadratic_simple(y[:, i, j])

    yy = fmriqa.detrend_quadratic(y)
    assert yy.shape == yyref.shape
    assert yy.dtype == yyref.dtype
    assert np.allclose(yy, yyref)
    assert np.allclose(y, y_orig)


def test_detrend_quadratic4():
    def _detrend_quadratic_simple(y):
        x = np.arange(len(y))
        return y - np.polyval(np.polyfit(x, y, deg=2), x)

    x = np.linspace(-10, 10, 100)
    x = np.linspace(-10, 10, 100)
    y = np.array([
        [[((-5.0 + 0.1 * k) * x**2 + (3.0 + 0.1 * j) * x + (2.0 + i)) for k in range(5)] for j in range(5)]
        for i in range(5)
    ]).T
    y += np.random.normal(loc=0.0, scale=0.1, size=y.shape)
    y_orig = y.copy()

    yyref = np.zeros_like(y)
    for i in range(5):
        for j in range(5):
            for k in range(5):
                yyref[:, i, j, k] = _detrend_quadratic_simple(y[:, i, j, k])

    yy = fmriqa.detrend_quadratic(y)
    assert yy.shape == yyref.shape
    assert yy.dtype == yyref.dtype
    assert np.allclose(yy, yyref)
    assert np.allclose(y, y_orig)


def test_read_dicom_objs_time_sorted_1():
    dobjs = fmriqa.read_dicom_objs_time_sorted(mosaic_test_series, '*.dcm')
    assert_equals(len(dobjs), 512)
    assert_equals(dobjs, sorted(dobjs, key=lambda d: d.AcquisitionTime))


def test_read_dicom_objs_time_sorted_2():
    dobjs = fmriqa.read_dicom_objs_time_sorted(multiframe_test_series, '*.dcm')
    assert_equals(len(dobjs), 1)


def test_read_dicom_objs_time_sorted_3():
    dobjs = fmriqa.read_dicom_objs_time_sorted(singleframe_test_series, '*.dcm')
    assert_equals(len(dobjs), 9728)
    assert_equals(dobjs, sorted(dobjs, key=lambda d: (d.TemporalPositionIdentifier, d.SliceLocation)))


def test_read_dicom_objs_time_sorted_4():
    assert_raises(ValueError, fmriqa.read_dicom_objs_time_sorted, mosaic_test_series, '*.XXX')


def test_read_dicom_objs_time_sorted_5():
    assert_raises(ValueError, fmriqa.read_dicom_objs_time_sorted, mosaic_test_series, '006D5BAD.dcm')


def test_read_dicom_objs_time_sorted_6():
    assert_raises(ValueError, fmriqa.read_dicom_objs_time_sorted, mosaic_test_series, 'MR23AD61F1.dcm')


def test_read_dicom_objs_time_sorted_7():
    dobjs = fmriqa.read_dicom_objs_time_sorted(hybrid_test_series, '*.dcm')
    assert_equals(len(dobjs), 600)
    assert_equals(dobjs, sorted(dobjs, key=fmriqa.acquisition_time_seconds))


def test_read_single_time_series_1():
    stacks, (dx, dy, dz, dt) = fmriqa.read_single_time_series(singleframe_test_series, '*.dcm')
    assert_equals(stacks.shape, (512, 19, 64, 64))
    assert_equals(stacks.dtype, np.float64)
    assert_almost_equals(dx, 3.4375, places=5)
    assert_almost_equals(dy, 3.4375, places=5)
    assert_almost_equals(dz, 8.0,    places=5)
    assert_almost_equals(dt, 2.0,    places=5)


def test_read_single_time_series_2():
    assert_raises(ValueError, fmriqa.read_single_time_series, mosaic_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_single_time_series, multiframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_single_time_series, hybrid_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_single_time_series, singleframe_test_series, '*.XXX')


def test_read_mosaic_time_series_1():
    stacks, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    assert_equals(stacks.shape, (512, 27, 64, 64))
    assert_equals(stacks.dtype, np.float64)
    assert_almost_equals(dx, 3.4375, places=5)
    assert_almost_equals(dy, 3.4375, places=5)
    assert_almost_equals(dz, 5.0,    places=5)
    assert_almost_equals(dt, 2.0,    places=5)

def test_read_mosaic_time_series_2():
    assert_raises(ValueError, fmriqa.read_mosaic_time_series, singleframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_mosaic_time_series, multiframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_mosaic_time_series, hybrid_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_mosaic_time_series, mosaic_test_series, '*.XXX')


def test_read_enhanced_time_series_1():
    stacks, (dx, dy, dz, dt) = fmriqa.read_enhanced_time_series(multiframe_test_series, '*.dcm')
    assert_equals(stacks.shape, (600, 27, 64, 64))
    assert_equals(stacks.dtype, np.float64)
    assert_almost_equals(dx, 3.4375, places=5)
    assert_almost_equals(dy, 3.4375, places=5)
    assert_almost_equals(dz, 5.5,    places=5)
    assert_almost_equals(dt, 2.0,    places=5)

def test_read_enhanced_time_series_2():
    assert_raises(ValueError, fmriqa.read_enhanced_time_series, singleframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_enhanced_time_series, mosaic_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_enhanced_time_series, multiframe_test_series, '*.XXX')
    assert_raises(ValueError, fmriqa.read_enhanced_time_series, hybrid_test_series, '*.XXX')


def test_read_hybrid_time_series_1():
    stacks, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, '*.dcm')
    assert_equals(stacks.shape, (600, 31, 64, 64))
    assert_equals(stacks.dtype, np.float64)
    assert_almost_equals(dx, 3.4375, places=5)
    assert_almost_equals(dy, 3.4375, places=5)
    assert_almost_equals(dz, 5.0,    places=5)
    assert_almost_equals(dt, 2.0,    places=5)

def test_read_hybrid_time_series_2():
    assert_raises(ValueError, fmriqa.read_hybrid_time_series, singleframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_hybrid_time_series, mosaic_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_hybrid_time_series, multiframe_test_series, '*.dcm')
    assert_raises(ValueError, fmriqa.read_hybrid_time_series, hybrid_test_series, '*.XXX')


# TODO: how do we test we've got the right bit in the phantom?
def test_get_roi():
    roisize = 21
    stacks, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:, 20, :, :]
    roi = fmriqa.get_roi(central_slice_time_series, roisize)
    nt, ny, nx = len(stacks), roisize, roisize
    assert_equals(roi.shape, (nt, ny, nx))


def test_signal_image():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:,25,:,:]
    signal = fmriqa.signal_image(central_slice_time_series)
    assert_equals(sha1(signal).hexdigest(), '674bc8c72749a271ec103ab1ff5100e266957301')

def test_temporalnoise_fluct_image():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:,25,:,:]
    tnf = fmriqa.temporalnoise_fluct_image(central_slice_time_series)
    assert_equals(sha1(tnf).hexdigest(), '9ca24d7ca22503c1b39ce712da7e678e047a8264')


def test_sfnr_image():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:,25,:,:]
    sfnr = fmriqa.sfnr_image(central_slice_time_series)
    assert_equals(sha1(sfnr).hexdigest(), 'ca30a04a1ad5e2eae41bbbac642ca0dfae796d25')


def test_sfnr_summary():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:,25,:,:]
    sfnr = fmriqa.sfnr_summary(central_slice_time_series)
    assert_almost_equals(sfnr, 719.805593640793)


def test_static_spatial_noise_image():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    central_slice_time_series = stacks[:,25,:,:]
    diff_image = fmriqa.static_spatial_noise_image(central_slice_time_series, mask_background=True)
    assert_equals(sha1(diff_image).hexdigest(), '03912ce66c0751e170f43892f91f2964be9376f2')


def test_snr_summary():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    snr = fmriqa.snr_summary(stacks[:,25,:,:])
    assert_almost_equals(snr, 818.186281422362)


def test_fluctuation_and_drift():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    (sd_resids, percent_fluct, drift_raw, drift_fit) = fmriqa.fluctuation_and_drift(stacks[:,25,:,:])
    assert_almost_equals(sd_resids, 0.8377229909631426)
    assert_almost_equals(percent_fluct, 0.030419931156640368)
    assert_almost_equals(drift_raw, 0.519740060879808)
    assert_almost_equals(drift_fit, 0.4068364561641941)


def test_magnitude_spectrum():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    spectrum = fmriqa.magnitude_spectrum(stacks[:,25,:,:])


def test_weisskoff():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    roc, covs = fmriqa.weisskoff(stacks[:,25,:,:])


def test_centre_of_mass():
    stacks, delta_t = fmriqa.read_mosaic_time_series(mosaic_test_series, '*.dcm')
    c_of_ms = fmriqa.centre_of_mass(stacks)
    assert_allclose(np.mean(c_of_ms, axis=0), [32.187927, 32.35453 , 13.748175], rtol=1e-3)
    assert_allclose(np.std(c_of_ms, axis=0), [0.005507, 0.011421, 0.006574], rtol=1e-3)


def test_phantom_mask_2d():
    image = dcmread(mosaic_test_file).pixel_array
    mask = fmriqa.phantom_mask_2d(image)
    assert_equals(mask.dtype, np.bool)
    assert_equals(mask.shape, image.shape) 


def test_ghost_mask():
    mask = fmriqa.phantom_mask_2d(dcmread(mosaic_test_file).pixel_array)
    gmask = fmriqa.ghost_mask(mask)
    assert_equals(gmask.dtype, np.bool)
    assert_equals(gmask.shape, mask.shape) 
    gmask = fmriqa.ghost_mask(mask, pe_axis='row')
    assert_equals(gmask.dtype, np.bool)
    assert_equals(gmask.shape, mask.shape) 


def test_volume_ghostiness():
    timeseries, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series)
    pmean, gmean, bright_gmean, snr = fmriqa.volume_ghostiness(timeseries[0])
    pmean, gmean, bright_gmean, snr = fmriqa.volume_ghostiness(timeseries[0], pe_axis='row')


def test_ghostiness_trends():
    timeseries, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series)
    pmeans, gmeans, bright_gmeans, snrs = fmriqa.ghostiness_trends(timeseries)

    timeseries, (dx, dy, dz, dt) = fmriqa.read_enhanced_time_series(multiframe_test_series, '*.dcm')
    pmeans, gmeans, bright_gmeans, snrs = fmriqa.ghostiness_trends(timeseries)

    timeseries, (dx, dy, dz, dt) = fmriqa.read_single_time_series(singleframe_test_series, ['*'])
    pmeans, gmeans, bright_gmeans, snrs = fmriqa.ghostiness_trends(timeseries)

    timeseries, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, ['*'])
    pmeans, gmeans, bright_gmeans, snrs = fmriqa.ghostiness_trends(timeseries)


def test_phantom_mask_3d():
    timeseries, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, '*.dcm')
    mask = fmriqa.phantom_mask_3d(np.sum(timeseries, axis=0))
    assert_equals(mask.dtype, np.bool)
    assert_equals(mask.shape, timeseries.shape[1:])


def test_smoothness_along_axis():
    timeseries, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series, ['*'])
    fwhmx = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 2, dx)
    fwhmy = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 1, dy)
    fwhmz = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 0, dz)
    assert_allclose(fwhmx, 8.65145, rtol=1e-3)
    assert_allclose(fwhmy, 9.20998, rtol=1e-3)
    assert_allclose(fwhmz, 37.8051, rtol=1e-3)

    timeseries, (dx, dy, dz, dt) = fmriqa.read_enhanced_time_series(multiframe_test_series, ['*'])
    fwhmx = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 2, dx)
    fwhmy = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 1, dy)
    fwhmz = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 0, dz)
    assert_allclose(fwhmx, 11.34925, rtol=1e-3)
    assert_allclose(fwhmy, 11.53056, rtol=1e-3)
    assert_allclose(fwhmz, 14.16524, rtol=1e-3)

    timeseries, (dx, dy, dz, dt) = fmriqa.read_single_time_series(singleframe_test_series, ['*'])
    fwhmx = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 2, dx)
    fwhmy = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 1, dy)
    fwhmz = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 0, dz)
    assert_allclose(fwhmx, 12.75856, rtol=1e-3)
    assert_allclose(fwhmy, 12.89833, rtol=1e-3)
    assert_allclose(fwhmz, 16.42438, rtol=1e-3)

    # Currently broken with existing test data
    #timeseries, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, ['*'])
    #fwhmx = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 2, dx)
    #fwhmy = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 1, dy)
    #fwhmz = fmriqa.smoothness_along_axis(timeseries[len(timeseries)//2], 0, dz)
    #assert_allclose(fwhmx, 0, rtol=1e-3)
    #assert_allclose(fwhmy, 0, rtol=1e-3)
    #assert_allclose(fwhmz, 0, rtol=1e-3)


def test_fwhm_smoothness_xyz():
    timeseries, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz(timeseries, (dx ,dy, dz))

    timeseries, (dx, dy, dz, dt) = fmriqa.read_enhanced_time_series(multiframe_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz(timeseries, (dx ,dy, dz))

    timeseries, (dx, dy, dz, dt) = fmriqa.read_single_time_series(singleframe_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz(timeseries, (dx ,dy, dz))

    # Currently broken with existing test data
    #timeseries, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, ['*'])
    # fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz(timeseries, (dx ,dy, dz))


def test_fwhm_smoothness_xyz_preprocessed():

    timeseries, (dx, dy, dz, dt) = fmriqa.read_mosaic_time_series(mosaic_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz_preprocessed(timeseries, (dx ,dy, dz))
    assert_greater(sum(fwhmx > 0), 0.98 * len(fwhmx))
    assert_greater(sum(fwhmy > 0), 0.90 * len(fwhmy))
    assert_greater(sum(fwhmz > 0), 0.80 * len(fwhmz))

    timeseries, (dx, dy, dz, dt) = fmriqa.read_enhanced_time_series(multiframe_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz_preprocessed(timeseries, (dx ,dy, dz))
    assert_greater(sum(fwhmx > 0), 0.98 * len(fwhmx))
    assert_greater(sum(fwhmy > 0), 0.90 * len(fwhmy))
    # NB TODO: something odd at the start and the end in the Philips data
    assert_greater(sum(fwhmz > 0), 0.50 * len(fwhmz))

    timeseries, (dx, dy, dz, dt) = fmriqa.read_single_time_series(singleframe_test_series, ['*'])
    fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz_preprocessed(timeseries, (dx ,dy, dz))
    assert_greater(sum(fwhmx > 0), 0.98 * len(fwhmx))
    assert_greater(sum(fwhmy > 0), 0.90 * len(fwhmy))

    # NB TODO: lots of erratic drop out in the GE data
    assert_greater(sum(fwhmz > 0), 0.35 * len(fwhmz))

    # Currently broken with existing test data
    #timeseries, (dx, dy, dz, dt) = fmriqa.read_hybrid_time_series(hybrid_test_series, ['*'])
    #fwhmx, fwhmy, fwhmz = fmriqa.fwhm_smoothness_xyz_preprocessed(timeseries, (dx ,dy, dz))
    #assert sum(fwhmx > 0) > 0.98 * len(fwhmx)
    #assert sum(fwhmy > 0) > 0.98 * len(fwhmy)
    #assert sum(fwhmz > 0) > 0.90 * len(fwhmz)
