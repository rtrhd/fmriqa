#!/usr/bin/env python
from __future__ import print_function

from setuptools import setup

from setuptools.command.build_py import build_py
from distutils.cmd import Command, log
from glob import glob
import os
from os.path import split, join, basename, dirname, isfile
from subprocess import check_call
import sys
from shutil import copyfile
import re


# single version
with open(join(dirname(__file__), 'fmriqa', 'version.py')) as f:
    exec(f.read())


def readme(fname):
    with open(join(dirname(__file__), fname)) as f:
        text = f.read()
    return text


# nb seems to run with unchanged cwd and these paths are relative
package_name = 'fmriqa'
resource_dir = 'resources'
dependencies = [
    'numpy>=1.8.2',
    'scipy>=0.13.3',
    'scikit-image>=0.10.1',
    'pydicom>=0.9.9',
    'psycopg2>=2.4.5',
    'QtPy>=1.1.2',
    'dcmfetch>=0.1.19'
]
if sys.version_info < (2, 7):
    dependencies.append('ordereddict')

dependency_links = [
    'git+https://bitbucket.org/rtrhd/dcmfetch.git'
]

def _which(program):
    def as_exe(fpath):
        """Returns executable path or None"""
        if os.name == 'nt':
            # explicit extension
            if fpath.lower().endswith(('.exe', '.bat', '.cmd')):
                return fpath if isfile(fpath) else None 
            else:
                for ext in ['.exe', '.bat', '.cmd']:
                    if isfile(fpath + ext):
                        return fpath + ext
                return None
        else:
            return fpath if (isfile(fpath) and os.access(fpath, os.X_OK)) else None

    # explicit path
    head, tail = split(program)
    if head:
        return as_exe(program)

    # search on system path
    for path in [dirname(sys.executable)] + os.environ["PATH"].split(os.pathsep):
        path = path.strip('"')
        exe = as_exe(join(path, program))
        if exe:
            return exe

    return None


def pyrcc(qrcfile, pyfile):
    commandpath = _which('pyrcc5')
    if commandpath:
        print(commandpath)
        print(commandpath, '-o', pyfile, qrcfile)
        return check_call([commandpath, '-o', pyfile, qrcfile])
    commandpath = _which('pyrcc4')
    if commandpath:
        return check_call([commandpath, '-py3', '-o', pyfile, qrcfile])
    commandpath = _which('pyside-rcc')
    if commandpath:
        return check_call([commandpath, '-py3', '-o', pyfile, qrcfile])
    raise OSError('No pyrcc command found')


def fix_imports(pyfile):
    # Replace toolkit specific import statement with a generic import
    # modifies file in place
    with open(pyfile) as f:
        lines = f.read().splitlines()
        for i, line in enumerate(lines):
            if re.search(r'import\s+QtCore', line):
                line, _ = re.subn(r'^from\s+\w+', r'from qtpy', line)
                lines[i] = line

    with open(pyfile, 'w') as f:
        for line in lines:
            print(line, file=f)


# The following is to run the qt resource compiler to build the
# python resource files for the embedded icon images.
class CompileResources(Command):
    description = 'Compile qt resources from rc file'

    def initialize_options(self):
        self.qrcfiles = glob(join(dirname(__file__), resource_dir, '*.qrc'))

    def finalize_options(self):
        pass

    def run(self):
        for qrcfile in self.qrcfiles:
            pyfile = qrcfile.replace('.qrc', '.py')
            target = join(dirname(__file__), package_name, basename(pyfile))

            self.announce('compiling resource file %s' % qrcfile, level=log.INFO)
            pyrcc(qrcfile, pyfile)

            self.announce('fixing up resource imports in %s' % pyfile, level=log.INFO)
            fix_imports(pyfile)

            self.announce('copying %s -> %s' % (pyfile, target), level=log.INFO)
            copyfile(pyfile, target)


class BuildWithResources(build_py):
    def run(self):
        self.run_command('compile_res')
        build_py.run(self)


setup(
    name=package_name,
    cmdclass={
        'compile_res': CompileResources,
        'build_py': BuildWithResources,
    },
    version=__version__,
    description='Tools for fMRI QA at CRIC based on FBIRN',
    long_description=readme('README.md'),
    author='Ronald Hartley-Davies',
    author_email='R.Hartley-Davies@bristol.ac.uk',
    license='MIT',
    url='https://bitbucket.org/rtrhd/frmiqa',
    download_url='https://bitbucket.org/rtrhd/frmiqa/downloads',
    packages=[package_name],
    install_requires=dependencies,
    dependency_links=dependency_links,
    tests_require=['nose'],
    keywords="fmri qa fbirn",
    entry_points={'gui_scripts': ['fbirnqa = fmriqa.fbirnqatool:main']},
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Medical Science Apps.",
    ]
)
