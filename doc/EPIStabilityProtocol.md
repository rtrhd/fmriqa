# Protocol for Phantom Assessment of Stability for fMRI #

## Introduction ##
It is intended that this scanning protocol should be carried out periodically (1-2 weeks) and following any Siemens service visit or intervention.
There is spherical unifromlly filled QA phantom for this purpose kept in the scanner room. (NB the phantom contents have been replace with a loose Carageenan Gel doped with Manganese rather than agarose/Nickel but these phantom shape and the scan protocol used remain the same as before)

The filler on top of the phantom is now a small plastic screw and nut. There is usually a small air bubble here. If it grows significantly the phantom can be topped up with deionized water.

![phantom on desk](phantom_on_desk.jpeg)

## Phantom Positioning ##
The scanning is carried out using the 32 channel head coil as this is the most commonly used for fMRI experiments.

The phantom should be positioned in the coil with the filler hole uppermost.
Sufficient padding should be used to position the sphere approximately at the magnet isocentre. Positioning errors of the order of 1-2cm are acceptable
but if the phantom is found from the localizer to be further than that from the isocentre then it should be repositioned.

![phantom in scanner](phantom_in_scanner.jpeg)

## Patient Registration ##
A patient should be registered as follows:

|             |              |
|-------------|--------------|
|*Name*       |FBIRN Phantom |
|*Patient ID* |FMRIQAYYYYMMDD|
|*Sex*        |Other         |
|*DOB*        |1-Jan-2001|
|*Weight*     |50kg|
|*Height*     |150cm|
|*Orientation*|HFS|
|*Body Part*  |Head|

Where *YYYYMMDD* is the date of the scan in *iso* format, *eg* 20130508 for the 8th of March 2013. This may be optionally followed by the initials of the operator. The operator field on the registration page should also be filled in appropriately. If this scheme is followed it is easy to find the scans on the DICOM server for analysis.

## Protocol Selection ##
There is a protocol under the Physics section called `fMRIQA/FBIRN Phantom`. This includes a localizer to check the phantom alignment and
a `epi2d` sequence that runs for approximately 20 mins. It has the following parameters:

|TR   |TE|FoV|Matrix|BW  |Frames|Slices|Spacing|Coverage|&alpha;|Phase BW|Shim Mode|iPAT|DistortionCorrn|Filter|FatSat|
|-----|--|---|------|----|------|------|-------|--------|-------|--------|---------|----|---------------|------|------|
|2000 |30|220|64*64 |3005|512   |27    |4+1    |135     |90&deg;|50?     |Advanced |None|Off            |None  |None  |

<img src="fmriphantom_prescribe.png" alt="fmri phantom prescribe" style="width: 65%;"/>

Use the localizer to get the phantom approximately at the magnet isocentre. Then adjust the position of the main sequence so it is centred on the phantom. In particular that the head to foot coverage is synmmetrical so that the whole phantom is included. The number of slices, T,,R,, and so on should be kept fixed.
  
## Additional Scans ##
There are two other shorter scans in the protocol which have different flip angles. These should be run directly after the main stability scan. These are intended to be used to separate out sources of temporal noise and compare them with physiological fluctuations. If they are copied over in the run area together with the main scan they will inherit any position adjustments that are made.

## Data transfer ##
The study should be archived to the DICOM store on `canopus`. It may be deleted after that.

## Analysis ##
The analysis will be carried on `canopus` using a [wiki:EPIStabilityGUITool tool provided for the purpose] and the results stored in a database.
