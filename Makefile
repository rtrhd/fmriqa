.PHONY: build install resources html tests patch clean
build:
	python setup.py build

install:
	python setup.py install

resources:
	(cd resources; make)

fmriqa/icon_resources.py: resources/icon_resources.qrc
	./resources/pyrcc-generic -o fmriqa/icon_resources.py resources/icon_resources.qrc
	./resources/fix-imports fmriqa/icon_resources.py

install-standalone: fmriqa/icon_resources.py fmriqa/
	./merge_version fmriqa/fbirnqatool.py fmriqa/fbirnqatool.py_versioned 
	./merge_resources fmriqa/fbirnqatool.py_versioned resources/icon_resources.qrc bin/fbirnqatool
	rm -f fmriqa/fbirnqatool.py_versioned
	sudo cp bin/fbirnqatool /usr/local/bin/fbirnqa
	sudo chmod +x /usr/local/bin/fbirnqa

install-web:
	(cd web; make install)

html:
	(cd ipynb; make html)

tests:
	(cd tests; make tests)

patch: fmriqa_phantomqa.pl.patch
	-sudo patch -N -r - -d /usr/local/bxh/bin < fmriqa_phantomqa.pl.patch

clean:
	(cd resources; make clean)
	rm -f bin/fbirnqatool fmriqa/icon_resources.py
	rm -rf build/ dist/
	(cd ipynb; make clean)
	rm -rf */*.pyc */__pycache__
	rm -rf fmriqa.egg-info
	(cd tests; make clean)

docker:
	docker build -t rtrhd/fmriqa .

