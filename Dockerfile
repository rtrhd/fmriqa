# Dockerfile for testing installation under neurodebian/ubuntu

from neurodebian:xenial

# rewrite sources list as contrib and non-free not included
RUN echo 'deb http://neuro.debian.net/debian xenial main contrib non-free' > /etc/apt/sources.list.d/neurodebian.sources.list
RUN echo 'deb http://neuro.debian.net/debian data main contrib non-free' >> /etc/apt/sources.list.d/neurodebian.sources.list

RUN apt-get update && apt-get install -y \
    curl \
    default-jre \
    git \
    make \
    python-dicom \
    python-nibabel \
    python-nose \
    python-numpy \
    python-pip \
    python-psycopg2 \
    python-setuptools \
    python-scipy \
    python-skimage \
    pyqt4-dev-tools \ 
    zip \
&& rm -rf /var/lib/apt/lists/*

RUN pip install qtpy git+https://bitbucket.org/rtrhd/dcmfetch

# To avoid warnings when importing dcmfetch
RUN curl -L https://sourceforge.net/projects/dcm4che/files/dcm4che3/3.3.7/dcm4che-3.3.7-bin.zip/download > /tmp/dcm4che3.zip; \
    unzip -d /usr/local/ /tmp/dcm4che3.zip; mv /usr/local/dcm4che-3.3.7 /usr/local/dcm4che3; rm /tmp/dcm4che3.zip

RUN useradd --create-home fmriqauser
COPY . /home/fmriqauser/fmriqa
RUN chown -R fmriqauser /home/fmriqauser/fmriqa

WORKDIR /home/fmriqauser/fmriqa
RUN python setup.py install

USER fmriqauser
